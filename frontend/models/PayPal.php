<?php
/**
 * Created by PhpStorm.
 * User: nestor
 * Date: 14.12.16
 * Time: 2:50
 */

namespace frontend\models;

use Yii;
use yii\base\Model;

class PayPal extends Model
{
    /*Выводим данные по поводу PayPal*/
    function secret_key(){
        $rows = (new \yii\db\Query())
            ->select(['Client_ID','Secret'])
            ->from('paypal')
            ->all();

        return $rows;
    }
}