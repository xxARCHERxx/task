<?php
/**
 * Created by PhpStorm.
 * User: nestor
 * Date: 14.12.16
 * Time: 2:27
 */

namespace frontend\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file','extensions' => ['zip']],
        ];
    }
}